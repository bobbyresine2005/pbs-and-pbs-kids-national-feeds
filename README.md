# PBS And PBS Kids National Feeds

national feeds for the pbs streams
local feeds are at https://i.mjh.nz/PBS

For Use With
KODI (Recommended for MPD/DRMs)
VLC Media Player (Does Not Support MPDs and DRMs)
MPC-HC (Live Feed & HLS Only)
And VideoJS Players like https://videojs-http-streaming.netlify.app.


For VideoJS-HTTP-Streaming, you need to put in the keySystems JSON code:

{"com.widevine.alpha": "https://proxy.drm.pbs.org/license/widevine/*-dash"}
